# simh-vax730-VMS730: Simulation of DEC VAX-11/730 with fresh OpenVMS VAX v7.3 installation

This is a pre-configured [SIMH](https://github.com/simh/simh) instance to simulate a [Digital Equipment Corporation (DEC)](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation) [VAX-11/730](https://en.wikipedia.org/wiki/VAX-11#VAX-11/730) computer system with a fresh installation of the [OpenVMS v7.3](https://en.wikipedia.org/wiki/OpenVMS) operating system. It also includes an image of the OpenVMS VAX v7.3 distribution CD-ROM that was used to install the operating system, and an overview of how the system was installed and set up. This is intended as a working example of how to begin setting up a SIMH simulation of a VAX computer running OpenVMS.

**This repository does not include any OpenVMS Product Authorization Keys (PAKs).** The operating system will complain about being unlicensed at boot time, it will not allow the use of any layered products, networking cannot be started, and it will only allow a user to log in on the console. The author of this repository cannot provide PAKs.

I have archived the OpenVMS 7.3 documentation [here](https://gitlab.com/NF6X_VAXen/OpenVMS-v7.3-docs) for convenience.

Here is a picture of my VAX-11/730 system. The right rack cabinet contains (from top to bottom) an RL02 removable-pack hard drive, the VAX-11/730 computer cabinet, and an R80 fixed hard drive. The left rack cabinet contains a TU80 magtape drive which is not included in this simulation... but you can add one if you like! Pay no attention to the other clutter in that photo, like the Data General Nova 3 system lurking in the background.

![Photo of my VAX-11/730 System](VAX-11-730-small.jpg)

You can see more about my system [here](https://www.nf6x.net/2014/05/nothing-sucks-power-like-a-vax/).


---

## System Configuration

I configured this example system with these settings. You will probably have to change some or all of them for practical use of this simulation.

Description              | Default setting
-------------------------|------------------
SIMH network interface   | vde:/var/run/vde2/tap1.ctl
System disk volume label | OVMSVAXSYS
SCSNODE                  | VMS730
SCSSYSTEMID              | 11994 (11*1024 + 730, i.e. DECnet address = 11.730)
Time zone                | GMT
Telnet listen port       | 11730

Three system accounts are created during installation. You should change all of these passwords.

Username | Password
---------|-----------------
SYSTEM   | overlord
SYSTEST  | testament
FIELD    | circusengineer

I selected these optional software packages during installation:

* OpenVMS library
* OpenVMS Help Message
* DECnet Phase IV networking

I did not install these optional software packages:

* OpenVMS optional
* OpenVMS Management Station
* DECwindows base support
* DECwindows workstation support
* DECnet-Plus networking

DECnet Phase IV networking has been installed but not configured or started. It requires appropriate PAKs before it can be started. If you have valid PAKs, then you may:

1. Change the SCSNODE and SCSSYSTEMID to suit your network
2. Configure DECnet Phase IV with the command `@SYS$MANAGER:NETCONFIG.COM`
3. Start the network with the command `@SYS$MANAGER:STARTNET.COM`

SCSNODE (the host name) and SCSSYSTEMID (an ID number which you normally calculate based on the system's DECnet address) are normally set during the operating system installation. Changing them after installing the operating system is a little bit involved. See [5.7 How do I change the node name of an OpenVMS System?](http://www.hoffmanlabs.com/vmsfaq/vmsfaq_007.html#mgmt9) in the [OpenVMS FAQ](http://www.hoffmanlabs.com/vmsfaq/vmsfaq.html) for instructions.

The time zone can be reconfigured by running the command `@SYS$MANAGER:UTC$TIME_SETUP.COM` and answering its prompts.


---

## Running the Simulation

Before you can run this simulation instance, you will need to download and install the [SIMH](https://github.com/simh/simh) software, particularly the `vax730` simulator. You will probably need to edit the `sysconfig.ini` initialization file provided here to configure networking for your host system. The initialization file assumes that you will be using a [Virtual Distributed Ethernet](https://github.com/virtualsquare/vde-2) switch with its control port located at `/var/run/vde2/tap1.ctl`. Depending on your host operating system, you may need to use a different network configuration. SIMH network configuration is described in the [0readme_ethernet.txt](https://github.com/simh/simh/blob/master/0readme_ethernet.txt) file provided with SIMH.

Once you have installed SIMH and edited `sysconfig.ini`, run the `vax730` program to start the simulation as a foreground process. It should load the `vax730.ini` file by default. `vax730.ini` loads the system configuration from `sysconfig.ini`, mounts a scratch pack on the RL02 (a 10Mbyte removable-pack hard drive), and boots the system from the R80 (a 120Mbyte non-removable hard drive). The VAX's console terminal will be in the foreground.

OpenVMS will print lots of stuff on the console while it boots. Once it finishes booting, the console should look something like this:

```
The OpenVMS VAX system is now executing the site-specific startup commands.

%SET-I-INTSET, login interactive limit = 64, current interactive value = 0
  SYSTEM       job terminated at 24-NOV-2021 14:47:45.65

  Accounting information:
  Buffered I/O count:            1122         Peak working set size:    1574
  Direct I/O count:               678         Peak page file size:      4665
  Page faults:                   3457         Mounted volumes:             0
  Charged CPU time:           0 00:00:03.49   Elapsed time:     0 00:00:08.06

```

At this point, press return to get a login prompt, and then log in as `SYSTEM` with password `overlord` (or whatver password you changed that to). It should look like this:

```
 Welcome to OpenVMS (TM) VAX Operating System, Version V7.3    

Username: system
Password: 
%LICENSE-I-NOLICENSE, no license is active for this software product
%LOGIN-S-LOGOPRCON, login allowed from OPA0:
 Welcome to OpenVMS (TM) VAX Operating System, Version V7.3
    Last interactive login on Wednesday, 24-NOV-2021 14:51
$ 
```

The simulation listens for telnet connections on port 11730 and treats them as connections to one of the VAX-11/730's eight asynchronous serial ports.


---

## Mounting RL02 Packs

The `vax730.ini` file spins up a preformatted RL02 pack labeled `SCRATCH` in the RL02 drive, but the operating system does not automatically mount it. You can manually mount it like this:

```
$ show dev dq

Device                  Device           Error    Volume         Free  Trans Mnt
 Name                   Status           Count     Label        Blocks Count Cnt
VMS730$DQA0:            Mounted              0  OVMSVAXSYS       64980   150   1
VMS730$DQA1:            Online               0
$ mount/system dqa1: SCRATCH
%MOUNT-I-MOUNTED, SCRATCH mounted on _VMS730$DQA1:
$ show dev dq

Device                  Device           Error    Volume         Free  Trans Mnt
 Name                   Status           Count     Label        Blocks Count Cnt
VMS730$DQA0:            Mounted              0  OVMSVAXSYS       64980   150   1
VMS730$DQA1:            Mounted              0  SCRATCH          20424     1   1
$ dir dqa1:[000000]

Directory DQA1:[000000]

000000.DIR;1        BACKUP.SYS;1        BADBLK.SYS;1        BADLOG.SYS;1       
BITMAP.SYS;1        CONTIN.SYS;1        CORIMG.SYS;1        INDEXF.SYS;1       
SECURITY.SYS;1      VOLSET.SYS;1        

Total of 10 files.
```

If you want to mount a different pack, first dismount the current pack under VMS if one is mounted. Then use ^e to suspend the VAX and get to the SIMH console prompt, change the mounted pack image with the `ATTACH` command in SIMH, and then use the `CONTINUE` command to resume the simulation:

```
$ dismount dqa1:
$ ^e
Simulation stopped, PC: 803E0D34 (CLRL R6)
sim> attach rb1 STANDBACKUP.rl02
sim> cont

$ mount/system dqa1: STANDBACKUP
%MOUNT-I-MOUNTED, STANDBACKUP mounted on _VMS730$DQA1:
$ dir dqa1:[000000]

Directory DQA1:[000000]

000000.DIR;1        BACKUP.SYS;1        BADBLK.SYS;1        BADLOG.SYS;1       
BITMAP.SYS;1        CONTIN.SYS;1        CORIMG.SYS;1        INDEXF.SYS;1       
ISL_SCRIPT.ESS;1    SECURITY.SYS;1      SYS0.DIR;1          VOLSET.SYS;1       

Total of 12 files.
```

---

## Creating Blank RL02 Packs

The simulated hardware is configured with the IDC703 Integrated Drive Controller, which supports one R80 fixed hard drive and up to three RL02 removable pack hard drives, all from a single card. That's great, and it's a typical VAX-11/730 configuration. In SIMH, it's the `RB` device and it presents devices `RB0` through `RB3`.

Now, SIMH will automatically create a new empty drive image when you `ATTACH` a nonexistent file to an emulated device. That's great, too! But there's a problem. The `RB` device doesn't seem to be creating the files correctly. They're half the size that they are supposed to be, and then when I try to `INITIALIZE` one in OpenVMS, the OS complains about not being able to find the final track which should contain bad block information.

There's an easy workaround. The `RL` device in SIMH, which simulates a regular RL11 disk controller, creates new images which satisfy OpenVMS when they are mounted on the `RB` device in SIMH. Here is how I create blank RL02 pack images at the `simh>` prompt:

```
sim> set rl enable
sim> set rl0 rl02
sim> attach rl0 NEWPACK.rl02
RL0: Creating new file: NEWPACK.rl02
Overwrite last track? [N]y
sim> set rl0 badblock
Overwrite last track? [N]y
sim> set rl disable
```

I disabled the `RL` device at the end because I didn't want the RL11 controller present in the simulated system when I booted it.


---

## Shutting Down the System

Once you get bored, log in as `SYSTEM` and perform a shutdown using the command `@SYS$SYSTEM:SHUTDOWN` (you can type it in lower case if you prefer) and answer all of the prompts (you can just keep pressing return to accept the defaults), and then wait for the VAX to shut down. Once it halts, you should be a SIMH's `sim>` prompt. Then, type `exit` to exit from SIMH. It should look something like this right before exiting to your shell prompt:

```
%SHUTDOWN-I-REMOVE, all installed images will now be removed
%SHUTDOWN-I-DISMOUNT, all volumes will now be dismounted
%%%%%%%%%%%  OPCOM  24-NOV-2021 15:02:19.86  %%%%%%%%%%%
Message from user SYSTEM on VMS730
_VMS730$OPA0:, VMS730 shutdown was requested by the operator.

%%%%%%%%%%%  OPCOM  24-NOV-2021 15:02:19.88  %%%%%%%%%%%
Logfile was closed by operator _VMS730$OPA0:
Logfile was VMS730::SYS$SYSROOT:[SYSMGR]OPERATOR.LOG;2

%%%%%%%%%%%  OPCOM  24-NOV-2021 15:02:19.90  %%%%%%%%%%%
Operator _VMS730$OPA0: has been disabled, username SYSTEM

    SYSTEM SHUTDOWN COMPLETE - use console to halt system
Infinite loop, PC: 802F84D3 (BRB 802F84D3)
sim> exit
Goodbye
Eth: closed vde:/var/run/vde2/tap1.ctl
```

---

## OpenVMS Installation

Here is an overview of how I installed OpenVMS v7.3 on this simulated system. You can find a complete log of the installation session in the [install.log](install.log) file.

1. I created the `install.ini` initialization file which loads the normal system configuration from `sysconfig.ini`, temporarily installs an MSCP drive controller card with an attached CD-ROM drive, and mounts the OpenVMS VAX v7.3 installation CD-ROM. It leaves me at the `simh>` prompt instead of booting the VAX, so that I can do stuff manually.

2. I manually created a couple of blank RL02 pack images for later use: One to install the VMS Standalone Backup kit on, and another one to use as a scratch disk pack.

3. I booted the VAX from the CD-ROM to start the installation process. The process begins by restoring just enough of a bootable OpenVMS installation onto the R80 hard drive to be able to perform the rest of the installation.

4. I booted the VAX from its R80 hard drive for the next stage of the installation process.

5. Once the second stage of installation completed, I booted the freshly-installed operating system from the R80.

6. I logged in, mounted the previously prepared `STANDBACKUP.rl02` pack on the RL02 drive, and then installed the OpenVMS Standalone Backup kit on it with the command `@SYS$UPDATE:STABACKIT`.

7. Next, I mounted the previously prepared `SCRATCH.rl02` pack on the RL02 drive and initialized it.

8. Finally, I shut down the operating system and got to work on this documentation.


---

## File Manifest

SIMH initialization files:

Filename               | Description
-----------------------|------------------------------------
install.ini            | Prepare the simulated system for OpenVMS installation
mount-cdrom.ini        | Mount the OpenVMS installation CD-ROM (normally called by other .ini files)
standbackup.ini        | Boot Standalone Backup from an RL02 pack
sysconfig.ini          | Set up the hardware configuration (normally called by other .ini files)
vax730.ini             | Default SIMH `vax730` initialization file: Boot from the R80 hard drive


Disk image files:

Filename               | Description
-----------------------|------------------------------------
OpenVMS_VAX_073_OS.iso | OpenVMS VAX v7.3 installation CD-ROM
OVMSVAXSYS.r80         | Main system hard drive with OpenVMS installation
SCRATCH.rl02           | A formatted RL02 pack for scratch use
STANDBACKUP.rl02       | Bootable RL02 pack with OpenVMS Standalone Backup


Other files:

Filename               | Description
-----------------------|------------------------------------
install.log            | Log of the system installation session
README.md              | This file. Right here. The one you are reading.
VAX-11-730-small.jpg   | A photo of my real VAX-11/730


---

## Former HPE OpenVMS Hobbyist Program

HPE (which owned remnants of Compaq, which in turn owned remnants of DEC) used to issue free PAKs to allow hobbyists to run OpenVMS on VAX and/or Alpha hardware. The last VAX licenses issued under that program expired by December 31, 2020, and there does not appear to be a proper way to acquire free VAX PAKs any more. When the program was still in effect, participants were issued PAKs for the following OS and layered products, with termination dates one year from the date of issue and limited to 100 concurrent uses:

* ACMS
* ACMS-REM
* ACMS-RT
* ACMSXP-DEV
* ACMSXP-RT
* ADA
* ADA-PDO
* ADAO-PDO
* ALLIN1-MAIL-DW-CLIENT
* ALLIN1-MAIL-SERVER
* ALLIN1-MAIL-SERVER-USER
* ALLIN1-MAIL-VT-CLIENT
* ALLIN1-MAIL-VT-USER
* ALLIN1-MAIL-WAN-SERVER 
* AUDIOKIT-USER
* AVAIL-MAN
* BASIC
* C
* CMS
* COBOL
* CXX-V
* DCE-APP-DEV
* DCE-CDS
* DCE-SECURITY
* DCPS-OPEN
* DCPS-PLUS
* DECDCS-SRV-VA
* DECMIGRATE
* DECRAM
* DECWRITE
* DECWRITE-USER
* DESKTOP-ACMS
* DFG
* DFS
* DQS
* DTM
* DTR
* DTR-UI-JAPANESE
* DVNETEND
* DVNETEXT
* DVNETRTG
* DW-MOTIF
* DW-MOTIF-UI-CESKY
* DW-MOTIF-UI-DEUTSCH
* DW-MOTIF-UI-ESPANOL
* DW-MOTIF-UI-FRANCAIS
* DW-MOTIF-UI-HANGUL
* DW-MOTIF-UI-HANYU
* DW-MOTIF-UI-HANZI
* DW-MOTIF-UI-HEBREW
* DW-MOTIF-UI-ITALIANO
* DW-MOTIF-UI-JAPANESE
* DW-MOTIF-UI-MAGYAR
* DW-MOTIF-UI-POLSKI
* DW-MOTIF-UI-RUSSKIJ
* DW-MOTIF-UI-SLOVENSKY
* DW-MOTIF-UI-SVENSKA
* DW-SNA-3270-TE-VMS
* EXT-MATH-LIB
* EXT-MATH-LIB-RT
* FMS
* FMS-RT-UI-JAPANESE
* FMS-UI-HANGUL
* FMS-UI-JAPANESE
* FORMS
* FORMS-RT
* FORMS-RT-UI-HANGUL
* FORMS-RT-UI-HANYU
* FORTRAN
* GKS
* GKS-RT
* GKS-RT-UI-JAPANESE
* GKS-UI-JAPANESE
* GKS3D
* GKS3D-RT
* LSE
* MACRO64
* MAILBUS-400-API
* MAILBUS-400-MTA
* MMOV-DV
* MMOV-RT
* MMS
* NOTES
* OPENVMS-ALPHA
* OPENVMS-ALPHA-USER
* OPENVMS-HOBBYIST
* OPS5
* PASCAL
* PCA
* PHIGS
* PHIGS-RUNTIME
* PHIGS-RUNTIME-UI-JAPAN
* PHIGS-UI-JAPANESE
* RMSJNL
* RTR-CL
* RTR-SVR
* SQL-DEV
* SSU
* UCX
* UCX-IP-CLIENT
* UCX-IP-NFS
* UCX-IP-RT
* VAX-VMS
* VAXCLUSTER
* VAXSET
* VMS-UI-JAPANESE
* VMSCLUSTER
* VOLSHAD
* X25
* X25-CLIENT
* X500-ADMIN-FACILITY
* X500-DIRECTORY-SERVER
